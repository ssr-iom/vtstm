Install Zim
sudo add-apt-repository ppa:jaap.karssenberg/zim
sudo apt-get update
sudo apt install zim

sudo apt install ffmpeg
sudo apt install xdotool

Python environment:

conda create -n littlehelper python=3.7
conda activate littlehelper
mamba install imageio xarray scipy matplotlib pyqt hvplot
mamba install -c nexpy nexusformat
pip install nxarray spym
