import os
import time

nas_prefix = "./../../../../../../"
abs_nas_prefix = "/mnt/stm-nas/"
log_path = os.path.join(nas_prefix, "VTSTM/Elogbooks/VT-STM/Logbook")
abs_log_path = os.path.join(abs_nas_prefix, "VTSTM/Elogbooks/VT-STM/Logbook")
zim_path = "zim"

def date2pagepath(date_str):
    """Convert date string into full page path

    Args:
            date_str: date string in the form "YYYYMMDD"

    Return: full path to the page.

    """

    y = date_str[0:4]
    m = date_str[4:6]
    d = date_str[6:8]
    return os.path.join(log_path, y, m, d + ".txt")

def date2abspagepath(date_str):
    """Convert date string into full page path

    Args:
            date_str: date string in the form "YYYYMMDD"

    Return: full path to the page.

    """

    y = date_str[0:4]
    m = date_str[4:6]
    d = date_str[6:8]
    return os.path.join(abs_log_path, y, m, d + ".txt")

def append(date_str, text):
    """Append some text to a specific page using "Append" plugin.
    
    Args:
            datestr: date of the page in the form "YYYYMMDD"
            text: text to be written. Wiki format supported.
    
    Returns: Output of "zim --plugin append" or an error.
    
    """

    page_reload()
    time.sleep(0.1)
    page_file = date2abspagepath(date_str)
    with open(page_file, "a") as f:
        f.write(text)
    time.sleep(0.1)
    page_reload()

def page2list(date_str):
    """Read logbook page and return a list of string

    Args:
            date_str: date string in the form "YYYYMMDD"

    Return: page content as string list, one for line

    """

    fullpath = date2abspagepath(date_str)
    with open(fullpath, "r") as f:
        page_list = f.read()

    return page_list

def load_sample(sampleName, measTime):## TO DO
    """Load metadata from a .zym logbook
    
    Args:
            sampleName: sample identifier in the form VTYYMMDD_XX.
            measTime: timestamp of the measurements.
    
    Returns: string containing logbook info
    
    """

    log = '-'

    # Calculate date of sample preparation
    sampleTime = None

    # Open logbook of the measurement day
    logFile = None#os.path.join(log_path, year, month, day + '.txt')

    #with open(logFile) as f:
    #    logbook = f.readlines()

    #for i,line in enumerate(logbook):
    #    if line.endswith(sampleName+' ==='):
    #            log += line

    return log

def page_reload():
    os.system('xdotool search --name "VT-STM - Zim" windowactivate')
    os.system('sleep 0.1')
    os.system('xdotool key ctrl+r')
