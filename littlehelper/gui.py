import datetime
from PyQt5.QtWidgets import QMainWindow, QWidget, QPushButton, QLineEdit, QLabel, QAction, QFileDialog
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSignal, QThread
import os

import LEED
import STM
import rhk

class Main(QMainWindow):
    def __init__(self):
        super(Main, self).__init__()
        self.initUi()

    def initUi(self):
        # Window properties
        self.setGeometry(1400, 550, 450, 300)
        self.setMinimumSize(self.size())
        self.setWindowTitle("Little Helper")
        self.setWindowIcon(QIcon("/usr/share/icons/breeze-dark/places/16/folder-favorites.svg"))

        # Create central widget
        self.centralWidget = Widget()
        self.setCentralWidget(self.centralWidget)

        # Create menu bar
        menubar = self.menuBar()

        # STM menu
        STMMenu = menubar.addMenu("STM")

        STMMenu_log_folder = QAction("Add overview from folder", self)        
        STMMenu_log_folder.setStatusTip('Insert overview into logbook from a selected folder.')
        STMMenu_log_folder.triggered.connect(self.centralWidget.STM_log_folder)

        STMMenu.addAction(STMMenu_log_folder)

        # LEED menu
        LEEDMenu = menubar.addMenu("LEED")

        LEEDMenu_log = QAction("Add to logbook", self, checkable=True)
        LEEDMenu_log.setStatusTip("Add LEED snapshot to logbook if today page exists. It will add just one entry for each energy.")
        LEEDMenu_log.setChecked(True)
        LEEDMenu_log.triggered.connect(self.centralWidget.toggleLEEDlog)

        LEEDMenu_avg = QAction("Average images", self)        
        LEEDMenu_avg.setStatusTip('Average LEED image files.')
        LEEDMenu_avg.triggered.connect(self.centralWidget.avgLEED)

        LEEDMenu.addAction(LEEDMenu_log)
        LEEDMenu.addAction(LEEDMenu_avg)

        # RHK menu
        RHKMenu = menubar.addMenu("RHK")

        RHKMenu_initialize = QAction("Initialize R9s", self)        
        RHKMenu_initialize.setStatusTip('Update save path and filenames.')
        RHKMenu_initialize.triggered.connect(self.centralWidget.RHK_initialize)

        RHKMenu.addAction(RHKMenu_initialize)

        # Create statusbar
        self.statusbar = self.statusBar()
        self.statusbar.showMessage("Ready")
        self.centralWidget.status[str].connect(self.statusbar.showMessage)

        self.show()

class Widget(QWidget):

    status = pyqtSignal(str)
    LEED_log = True

    def __init__(self):
        super(Widget, self).__init__()
        self.initUI()

    def initUI(self):
        
        # Sample textbox
        samplelabel = QLabel("Sample name:", self)
        samplelabel.move(10, 10)

        self.samplebox = QLineEdit(self)
        self.samplebox.move(10, 30)
        now = datetime.datetime.now()
        year = now.strftime("%y")
        month = now.strftime("%m")
        day = now.strftime("%d")
        self.samplebox.setText("VT" + year + month + day + "_A1")
        self.samplebox.resize(200,25)

        ## LOGBOOK section
        loglabel = QLabel("LOGBOOK", self)
        x_log = 10
        y_log = 70
        loglabel.move(x_log, y_log)

        # Update overview button
        self.UpdOverview_btn = QPushButton('Update overview', self)
        self.UpdOverview_btn.setToolTip('Update overview of new saved files into logbook')
        self.UpdOverview_btn.resize(110, 25)
        self.UpdOverview_btn.move(x_log, y_log+20)
        self.UpdOverview_btn.clicked.connect(self.STM_log_today)

        ## LEED section
        leedlabel = QLabel("LEED", self)
        x_LEED = 10
        y_LEED = 140
        leedlabel.move(x_LEED, y_LEED)

        # Energy textbox
        energylabel = QLabel("Energy (eV):", self)
        energylabel.move(x_LEED, y_LEED+20)
        self.energybox = QLineEdit(self)
        self.energybox.move(x_LEED, y_LEED+40)
        self.energybox.setText("60")
        self.energybox.resize(80,25)

        # Save LEED button
        self.leedbtn = QPushButton('Save snapshot', self)
        self.leedbtn.setToolTip('Save LEED snapshot. File name auto-increments.')
        self.leedbtn.resize(110, 25)
        self.leedbtn.move(x_LEED+90, y_LEED+40)
        self.leedbtn.clicked.connect(self.saveLEED)

        # Save LEED series Start/Stop buttons
        self.leedseries_start = QPushButton('Save series', self)
        self.leedseries_start.setToolTip('Save LEED snapshot series.')
        self.leedseries_start.resize(110, 25)
        self.leedseries_start.move(x_LEED+90, y_LEED+70)
        self.leedseries_start.clicked.connect(self.seriesLEED_start)

        self.leedseries_stop = QPushButton('Stop series', self)
        self.leedseries_stop.setToolTip('Stop current series.')
        self.leedseries_stop.resize(110, 25)
        self.leedseries_stop.move(x_LEED+90, y_LEED+70)
        self.leedseries_stop.setEnabled(False)
        self.leedseries_stop.setVisible(False)

    def STM_log_today(self):

        now = datetime.datetime.now()
        today_str = now.strftime("%Y%m%d")
        self.UpdOverview(today_str)

    def STM_log_folder(self):

        path = QFileDialog.getExistingDirectory(self,
                                                  "Select folder",
                                                  STM.STM_prefix,
                                                  QFileDialog.ShowDirsOnly | QFileDialog.DontResolveSymlinks
                                                  )

        folder = os.path.basename(path)
        if len(folder) == 8:
            self.UpdOverview(folder)
        else:
            self.status.emit("Selected folder is not valid.")

    def UpdOverview(self, date_str):
        self.UpdOverview_thread = STM.UpdateOverview(date_str, self.samplebox.text())
        self.UpdOverview_thread.status.connect(self.UpdOverview_status)
        self.UpdOverview_thread.finished.connect(self.UpdOverview_finished)
        self.UpdOverview_thread.start()
        self.UpdOverview_btn.setEnabled(False)

    def UpdOverview_status(self, status):
        self.status.emit(status)

    def UpdOverview_finished(self):
        self.UpdOverview_btn.setEnabled(True)

    def saveLEED(self):

        state = LEED.save(
            sample = self.samplebox.text(),
            energy = self.energybox.text(),
            log_checked = self.LEED_log
            )
        self.status.emit(state)

    def seriesLEED_start(self):
        
        self.LEEDseries_thread = LEED.LEEDseries(
            self.samplebox.text(),
            self.energybox.text()
            )
        self.LEEDseries_thread.status.connect(self.seriesLEED_status)
        self.LEEDseries_thread.finished.connect(self.seriesLEED_stop)
        self.LEEDseries_thread.start()
        self.leedseries_stop.setEnabled(True)
        self.leedseries_stop.setVisible(True)
        self.leedseries_stop.clicked.connect(self.LEEDseries_thread.terminate)
        self.leedseries_start.setEnabled(False)
        self.leedseries_start.setVisible(False)

    def seriesLEED_status(self, status):
        self.status.emit(status)

    def seriesLEED_stop(self):
        self.leedseries_stop.setEnabled(False)
        self.leedseries_stop.setVisible(False)
        self.leedseries_start.setEnabled(True)
        self.leedseries_start.setVisible(True)
        self.status.emit("LEED series done.")

    def avgLEED(self):

        fnames = QFileDialog.getOpenFileNames(self,
                                              'Select files to average',
                                              LEED.LEED_path,
                                              "Images (*.png *.jpg)"
                                              )

        if fnames:
            state = LEED.average(
                files = fnames[0]
                )
            self.status.emit(str(state))

    def toggleLEEDlog(self, state):
        self.LEED_log = state

    def RHK_initialize(self):

        now = datetime.datetime.now()
        yyyy = now.strftime("%Y")
        month = now.strftime("%m")
        day = now.strftime("%d")

        ## Save in local on acquisition PC (folders has to be created manually each day)
        #save_path = os.path.join("F:/data_folder", yyyy, yyyy + month + day, "STM")
        ## Save on server (folders are created automatically)
        save_path = os.path.join("Z:/VTSTM/data_folder", yyyy, yyyy + month + day, "STM")
        nas_path = os.path.join("/mnt/stm-nas/VTSTM/data_folder", yyyy, yyyy + month + day, "STM")
        if not os.path.exists(nas_path):
            os.makedirs(nas_path)
        ##

        save_path_out = rhk.send("SetSWSubItemParameter, Scan Area Window, MeasureSave, Save Path, " + save_path)
        self.status.emit("Setting path: " + save_path_out)
        file_name_out_STM = rhk.send("SetSWSubItemParameter, Scan Area Window, MeasureSave, File Name, " + self.samplebox.text())
        self.status.emit("Setting STM file name: " + file_name_out_STM)
        file_name_out_IV = rhk.send("SetSWSubItemParameter, IV Spectroscopy, MeasureSave, File Name, " + self.samplebox.text() + "_IV")
        self.status.emit("Setting IV file name: " + file_name_out_IV)
        file_name_out_IZ = rhk.send("SetSWSubItemParameter, IZ Spectroscopy, MeasureSave, File Name, " + self.samplebox.text() + "_IZ")
        self.status.emit("Setting IZ file name: " + file_name_out_IZ)
        file_name_out_dIdV = rhk.send("SetSWSubItemParameter, dI/dV Spectroscopy, MeasureSave, File Name, " + self.samplebox.text() + "_dIdV")
        self.status.emit("Setting dIdV file name: " + file_name_out_dIdV)

        if save_path_out == file_name_out_STM == file_name_out_IV == file_name_out_IZ == file_name_out_dIdV == "Done":
            self.status.emit("R9s initialization done.")
        else:
            self.status.emit("Error in R9s initialization!")
