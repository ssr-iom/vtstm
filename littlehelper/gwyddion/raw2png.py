### Python2 Gwyddion script called with 'gwyddion._gwy3("raw2png", filename, imgfile, channel, processes)'

import gwy
import sys

#sys.path.append("/usr/share/gwyddion/pygwy")
#import gwyutils

# Skipping sys.argv[0] which is simply the filename of this script.

# Retrieve variables
filename = sys.argv[1]
imgfile = sys.argv[2]
channel = int(sys.argv[3])

# Retrieve list of processes to apply
processes = sys.argv[4:]

def main():

    # Set up parameters for the 'align_rows' function
    settings = gwy.gwy_app_settings_get()
    settings['/module/linematch/direction'] = int(gwy.ORIENTATION_HORIZONTAL)
    settings['/module/linematch/do_extract'] = False
    settings['/module/linematch/do_plot'] = False
    settings['/module/linematch/method'] = 2 # {1:median, 2:median of differences, 3:modus, 4:matching}

    # Set up parameters fo image saving
    settings['/module/pixmap/active_page'] = 0
    settings['/module/pixmap/bg_color/alpha'] = 1
    settings['/module/pixmap/bg_color/blue'] = 1
    settings['/module/pixmap/bg_color/green'] = 1
    settings['/module/pixmap/bg_color/red'] = 1
    settings['/module/pixmap/border_width'] = 0
    settings['/module/pixmap/decomma'] = False
    settings['/module/pixmap/draw_frame'] = False
    settings['/module/pixmap/draw_mask'] = True
    settings['/module/pixmap/draw_maskkey'] = True
    settings['/module/pixmap/draw_selection'] = False
    settings['/module/pixmap/fix_fmscale_precision'] = False
    settings['/module/pixmap/fix_kilo_threshold'] = False
    settings['/module/pixmap/fmscale_gap'] = 1
    settings['/module/pixmap/fmscale_precision'] = 2
    settings['/module/pixmap/font'] = "Arial"
    settings['/module/pixmap/font_size'] = 14
    settings['/module/pixmap/inset_color/alpha'] = 1
    settings['/module/pixmap/inset_color/blue'] = 1
    settings['/module/pixmap/inset_color/green'] = 1
    settings['/module/pixmap/inset_color/red'] = 1
    settings['/module/pixmap/inset_draw_label'] = True
    settings['/module/pixmap/inset_draw_text_above'] = False
    settings['/module/pixmap/inset_draw_ticks'] = True
    settings['/module/pixmap/inset_length'] = "10 nm"
    settings['/module/pixmap/inset_outline_color/alpha'] = 1
    settings['/module/pixmap/inset_outline_color/blue'] = 1
    settings['/module/pixmap/inset_outline_color/green'] = 1
    settings['/module/pixmap/inset_outline_color/red'] = 1
    settings['/module/pixmap/inset_pos'] = 5
    settings['/module/pixmap/inset_xgap'] = 1
    settings['/module/pixmap/inset_ygap'] = 1
    settings['/module/pixmap/interpolation'] = 1
    settings['/module/pixmap/kilo_threshold'] = 1200
    settings['/module/pixmap/line_width'] = 0.25
    settings['/module/pixmap/linetext_color/alpha'] = 1
    settings['/module/pixmap/linetext_color/blue'] = 0
    settings['/module/pixmap/linetext_color/green'] = 0
    settings['/module/pixmap/linetext_color/red'] = 0
    settings['/module/pixmap/maptype'] = 4
    settings['/module/pixmap/mask_key'] = "Mask"
    settings['/module/pixmap/maskkey_gap'] = 1
    settings['/module/pixmap/mode'] = 0
    settings['/module/pixmap/outline_width'] = 0
    settings['/module/pixmap/pxwidth'] = 0.10000000000000001
    settings['/module/pixmap/scale_font'] = True
    settings['/module/pixmap/sel_color/alpha'] = 1
    settings['/module/pixmap/sel_color/blue'] = 1
    settings['/module/pixmap/sel_color/green'] = 1
    settings['/module/pixmap/sel_color/red'] = 1
    settings['/module/pixmap/sel_line_thickness'] = 0
    settings['/module/pixmap/sel_number_objects'] = True
    settings['/module/pixmap/sel_outline_color/alpha'] = 1
    settings['/module/pixmap/sel_outline_color/blue'] = 1
    settings['/module/pixmap/sel_outline_color/green'] = 1
    settings['/module/pixmap/sel_outline_color/red'] = 1
    settings['/module/pixmap/sel_point_radius'] = 1
    settings['/module/pixmap/selection'] = ""
    settings['/module/pixmap/tick_length'] = 1.5
    settings['/module/pixmap/title_gap'] = 0
    settings['/module/pixmap/title_type'] = 0
    settings['/module/pixmap/transparent_bg'] = False
    settings['/module/pixmap/units_in_title'] = False
    settings['/module/pixmap/xreal'] = 100
    settings['/module/pixmap/xyexponent'] = -6
    settings['/module/pixmap/xymeasureeq'] = True
    settings['/module/pixmap/xytype'] = 1
    settings['/module/pixmap/xyunit'] = "m"
    settings['/module/pixmap/yreal'] = 80
    settings['/module/pixmap/zexponent'] = -6
    settings['/module/pixmap/zoom'] = 0.5
    settings['/module/pixmap/zreal'] = 1
    settings['/module/pixmap/ztype'] = 1
    settings['/module/pixmap/zunit'] = "m"

    # Load filename into Gwyddion container
    container = gwy.gwy_file_load(filename, gwy.RUN_NONINTERACTIVE)

    # Add container to Gwyddion data browser
    gwy.gwy_app_data_browser_add(container)

    # Get list of channels in the container
    ids = gwy.gwy_app_data_browser_get_data_ids(container)

    # Select first channel
    gwy.gwy_app_data_browser_select_data_field(container, ids[channel])

    # Get channel metadata as dictionary
    meta = {}
    meta_container = container["/"+str(channel)+"/meta"]
    keys = meta_container.keys_by_name()
    for k in keys:
        v = meta_container[k]
        meta[k] = v

    # Apply processes
    for proc in processes:
        gwy.gwy_process_func_run(proc, container, gwy.RUN_IMMEDIATE)

    # Apply automatic color range with 'Gwyddion.net' gradient
    container["/"+str(channel)+"/base/palette"] = "Gwyddion.net"
    container["/"+str(channel)+"/base/range-type"] = 2

    # Save to png
    gwy.gwy_file_save(container, imgfile, gwy.RUN_NONINTERACTIVE)
    #gwyutils.save_dfield_to_png(container, '/0/data', imgfile, gwy.RUN_NONINTERACTIVE)

    # Remove container from Gwyddion data browser
    gwy.gwy_app_data_browser_remove(container)

    print(meta)

if __name__ == "__main__":
    main()
