### Python2 Gwyddion script called with '_wrapper("gwy3", gwy_function, arguments)'.

import gwy
import sys

sys.path.append("/usr/share/gwyddion/pygwy")
import gwyutils

# Skipping sys.argv[0] which is simply the filename of this script.
# Retrieve the name of gwy function to call
function = sys.argv[1]
# Retrieve other optional arguments
arguments = ", ".join(sys.argv[2:])

def main():
    # Evaluate function call and print output
    out = eval("gwy." + function + "(" + arguments + ")")
    print(out)

if __name__ == "__main__":
    main()
