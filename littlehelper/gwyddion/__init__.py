import subprocess, ast
import os

module_path = os.path.dirname(__file__)
py2_path = "/usr/bin/python2.7"

def _wrapper(*args):
    '''Launch a pyhton2 Gwyddion script and retrieve its output
    
    *args = First argument is the file name of the python2 Gwyddion script in the current directory to be executed.
            Following optional arguments are those passed to the same python2 Gwyddion script.
    
    Return: Literal evaluation of called python2 script standard output.
    
    Example
        _wrapper("gwy3", "gwy_version_major")
        executes 'gwy.gwy_version_major()'
        and returns '2'.
    '''

    #Convert tuple of arguments into a list compatible with check_output
    py2script_name = args[0]#First argument is py2script to call
    if not py2script_name.endswith(".py"):
        py2script_name+=".py"
    arguments = list(args[1:])#List of arguments for py2script
    # Retrieve string printed output from py2 script
    py2script_path = os.path.join(module_path, py2script_name)
    try:
        py2out = subprocess.check_output([py2_path, py2script_path] + arguments)
    except subprocess.CalledProcessError as e:
        print(e.output.decode())
        return {}
    # Evaluate printed output to data and return it
    py3out = ast.literal_eval(py2out.decode("ascii"))
    return py3out

# In the following are defined wrapper functions to _wrapper

def raw2png(*args):
    '''Load and process raw STM file. Save thumbnail as png image.

    filename = path to the raw data STM image.
    pngfile = path to the png file to be saved.
    channel = data channel to preview.
    processes = [optional] list of Gwyddion processes to be applied
    (e.g. "level", "align_rows", "fix_zero" and any 'proc' type fucntion listed in 'http://gwyddion.net/module-list-nocss.en.php').
    '''

    return _wrapper("raw2png", *args)

def gwy3(*args):
    '''Run a gwy function/module with the given arguments.

    function = name of the gwy function or module to call
    arguments = [optional] arguments to be passed to function
    '''

    return _wrapper("gwy3", *args)
