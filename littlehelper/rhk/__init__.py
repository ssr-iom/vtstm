import socket
import time

#IP_Address_R9_PC = '127.0.0.1'
IP_Address_R9_PC = '140.105.87.59'
TCP_Port_R9s = 12600

BUFFER_SIZE = 1024

def send(command):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IP_Address_R9_PC, TCP_Port_R9s))

    # KEEP THE WAIT TIME BELOW, otherwise the R9 will ignore your first command
    time.sleep(0.1)

    print ("Connected to socket \n")
    #command = "GetSWSubItemParameter, Scan Area Window, MeasureSave, Default Comment"
    command += "\n"

    s.send(command.encode())
    print ("Command sent \n")

    output = s.recv(BUFFER_SIZE)
    print ("Output received \n")
    s.close()

    print ("Received output:", output)
    return output.decode()

def log_meta(meta_dict):
    """Retrieve some metadata for the logbook
    
    Args:
            meta_dict: dictionary of metadata
    
    Returns: string with selected metadata formatted
    
    """

    s_date = meta_dict["Date"][9:14]
    s_bias = "%.3f V" % (float(meta_dict["Bias"].split(" ")[0]))
    s_current = "%.3f nA" % (abs(float(meta_dict["Current"].split(" ")[0])*1E9))
    s_angle = meta_dict["Rotation angle"]
    s_xoff = str(float(meta_dict["X offset"].split(" ")[0])*1E9) + " nm"
    s_yoff = str(float(meta_dict["Y offset"].split(" ")[0])*1E9) + " nm"
    s_xsize = "%d" % (float(meta_dict["X size"])*abs(float(meta_dict["X scale"]))*1E9)
    s_ysize = "%d" % (float(meta_dict["Y size"])*abs(float(meta_dict["Y scale"]))*1E9)

    meta1 = ""
    meta1 += s_date
    meta1 += " "
    meta1 += "(" + s_bias + ", " + s_current + ") "
    meta1 += "(" + s_xsize + "x" + s_ysize + " nm) "
    meta1 += "\n"

    meta2 = "(" + s_xoff + ", " + s_yoff + ") " + s_angle
    meta2 += "\n"

    return meta1, meta2
