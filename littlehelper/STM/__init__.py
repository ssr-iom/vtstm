import os
from PyQt5.QtCore import pyqtSignal, QThread

import zim
import spym
import tools

nas_prefix = "/mnt/stm-nas/"
rel_nas_prefix = "./../../../../../../"
STM_prefix = os.path.join(nas_prefix, "VTSTM/data_folder")
relSTM_prefix = os.path.join(rel_nas_prefix, "data_folder")
thumbnail_ext = ".jpg"
STM_ext = ".sm4"

class UpdateOverview(QThread):

    status = pyqtSignal(str)

    def __init__(self, date_str, sample):
        QThread.__init__(self)
        self.datestr = date_str
        self.sample = sample

    def __del__(self):
        self.wait()

    def run(self):

        date_str = self.datestr
        sample_name = self.sample

        # Check if log page exists
        pagefile = zim.date2abspagepath(date_str)
        if not os.path.exists(pagefile):
            self.status.emit("Page " + pagefile + " does not exist!")
            return

        ## Transfer files to server (from SCALA)
        #self.status.emit("Checking files on server...")
        #try:
        #    scala.backup_now(date_str)
        #except:
        #    self.status.emit("Error in tranferring files to server!")
        #    return

        # Retrieve STM files list
        file_list = []
        STM_path = os.path.join(STM_prefix, date_str[0:4], date_str, "STM")
        relSTM_path = os.path.join(relSTM_prefix, date_str[0:4], date_str, "STM")

        if not os.path.exists(STM_path):
            self.status.emit("Folder " + STM_path + " does not exist!")
            return
        for file in os.listdir(STM_path):
            if file.endswith(STM_ext) and file.startswith(sample_name) and len(file) == len(sample_name)+9:
                file_list.append(file)
        if not file_list:
            self.status.emit("No STM files for " + date_str + " date!")
            return

        # Sort file list by name
        tools.natural_sort(file_list)

        # Create thumbnails folder
        png_path = os.path.join(pagefile[:-4], "STM_thumbnails")
        if not os.path.exists(png_path):
            os.makedirs(png_path)

        # Read current logbook page
        current_page = zim.page2list(date_str)

        # Number of logged files
        num_log = 0

        # Initialize log string
        log_str = ""

        for f in file_list:

            filepath = os.path.join(STM_path, f)
            relfilepath = os.path.join(relSTM_path, f)
            filename = os.path.splitext(f)[0]
            png_file = os.path.join(png_path, filename + thumbnail_ext)

            # Check if filename is alreay logged
            check_str = "[[" + relfilepath + "|" + filename + "]]\n"
            if check_str in current_page:
                self.status.emit("File " + filename + " already logged.")
                continue

            # Increment number of logged files
            num_log += 1

            # Open file with spym
            f = spym.load(filepath)
            meta = ''
            try:
                tf = f.Topography_Forward
            except:
                continue

            # Create thumbnail and metadata
            tf.spym.align()
            tf.spym.plane()
            tf.spym.fixzero()
            p = tf.spym.plot()
            f = p.get_figure()
            f.savefig(png_file)
            ax = p.axes
            meta = ax.get_title()

            # Create log string
            log_str += "\n[[" + relfilepath + "|" + filename + "]]\n"
            #log_str += meta + "\n"
            log_str += "{{./STM_thumbnails/" + filename + thumbnail_ext + "?width=600}}\n"

            # Send file addition
            self.status.emit("Logging " + filename + ".")

        # Add to logbook. Note that logging works only if Zim notebook root folder is set to '//stm-nas/VT-STM/'
        if num_log:
            zim.append(date_str, log_str)
            self.status.emit("Logbook updated with " + str(num_log) + " file(s).")
        else:
            self.status.emit("Logbook already updated.")
