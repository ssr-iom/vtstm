import os
import datetime
import imageio
from PyQt5.QtCore import pyqtSignal, QThread

import zim

nas_prefix = "/mnt/stm-nas/VTSTM" #Linux
LEED_path = os.path.join(nas_prefix, "LEED")

def save(sample, energy, log_checked):
    now = datetime.datetime.now()
    year = now.strftime("%Y")
    month = now.strftime("%m")
    day = now.strftime("%d")

    ## Check if folder already exists otherwise create it
    today_str = now.strftime("%Y%m%d")
    LEED_fullpath = os.path.join(LEED_path, year, today_str)
    LEED_fullrelpath = os.path.join("./../../../../../../../VTSTM/LEED/", year, today_str)
    if not os.path.exists(LEED_fullpath):
        os.makedirs(LEED_fullpath)

    ## Save snapshot
    LEED_fullname = os.path.join(LEED_fullpath, sample + "_" + energy + "eV_0.png")
    LEED_fullrelname = os.path.join(LEED_fullrelpath, sample + "_" + energy + "eV_0.png")
    i=0
    while os.path.exists(LEED_fullname):
        i+=1
        LEED_fullname = os.path.join(LEED_fullpath, sample + "_" + energy + "eV_" + str(i) + ".png")
        LEED_fullrelname = os.path.join(LEED_fullrelpath, sample + "_" + energy + "eV_" + str(i) + ".png")
    # Execute ffmpeg to save snapshot
    try:
        os.system("/usr/bin/ffmpeg -v 0 -i /dev/video0 -f image2 -framerate 1 -video_size 720x576 -vframes 1 " + LEED_fullname)
        status = "Snapshot saved to "+ sample + "_" + energy + "eV_" + str(i) + ".png. "
    except:
        return "An error occurred while saving the snapshot!"

    ## Write into the logbook
    if log_checked:
        # Note that logging works only if Zim notebook root folder is set to '//stm-nas/VT-STM/'
        LEED_log = os.path.relpath(LEED_fullrelname, start=nas_prefix)

        LEED_string = "\r\n" + energy + " eV"
        LEED_string += "\r\n{{" + LEED_fullrelname + "?width=400}}"
        LEED_string += "\r\n[[" + LEED_fullrelname + "|" + sample + "_" + energy + "eV_" + str(i) + ".png]]\r\n"

        # Check if log page exists
        pagefile = zim.date2abspagepath(today_str)
        if not os.path.exists(pagefile):
            status += "Page " + today_str + " does not exist!"
        else:
            zim.append(today_str, LEED_string)

    return status

class LEEDseries(QThread):
    status = pyqtSignal(str)
    def __init__(self, sample, energy):
        QThread.__init__(self)
        self.sample = sample
        self.energy = energy
        self.interval = 2

    def __del__(self):
        self.wait()

    def run(self):
        now = datetime.datetime.now()
        year = now.strftime("%Y")
        month = now.strftime("%m")
        day = now.strftime("%d")

        ## Check if folder already exists otherwise create it
        today_str = now.strftime("%Y%m%d")
        LEED_seriespath = os.path.join(LEED_path, year, today_str, self.sample + "_" + self.energy + "eV_0")
        j=0
        while os.path.exists(LEED_seriespath):
            j+=1
            LEED_seriespath = os.path.join(LEED_path, year, today_str, self.sample + "_" + self.energy + "eV_" + str(j))
        os.makedirs(LEED_seriespath)

        ## Save snapshots series
        i=0
        while True:
            i+=1
            now = datetime.datetime.now()
            LEED_fullname = os.path.join(LEED_seriespath, str(i) + now.strftime("_%Y%m%d_%H%M%S.png"))
            # Execute ffmpeg to save snapshot
            try:
                os.system("/usr/bin/ffmpeg -v 0 -i /dev/video0 -f image2 -framerate 1 -video_size 720x576 -vframes 1 " + LEED_fullname)
                status = "Snapshot saved to " + self.sample + "_" + self.energy + "eV_" + str(j) + "/" + str(i) + now.strftime("_%Y%m%d_%H%M%S.png")
            except:
                return "An error occurred while saving the snapshot!"
            self.status.emit(status)
            self.sleep(self.interval)

def average(files):
    avg_array = imageio.imread(files[0])
    avg_array = 0.0
    for f in files:
        avg_array += imageio.imread(f)
    avg_array = avg_array/len(files)
    avg_filename = os.path.splitext(files[0])[0]+"_avg.png"
    imageio.imwrite(avg_filename, avg_array)
    return "Average saved into " + avg_filename + "."
