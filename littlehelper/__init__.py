#!/home/stras/mambaforge/envs/littlehelper/bin/python
# -*- coding: iso-8859-1 -*-

import sys
from PyQt5.QtWidgets import QApplication

from gui import Main

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ex = Main()
    sys.exit(app.exec_())
