import numpy as np
import os

def backup_now(date_str):
    """Backup data from SCALA workstation to NAS
    
    Args:
            date_str: name of the folder to backup
    
    Returns: status
    
    """

    remote_folder = os.path.join("/data/sample/newdata/daily_data/", date_str)
    nas_folder = os.path.join("/mnt/stm-nas/VTSTM/data_folder/", date_str[0:4], date_str, "STM")

    # Use wget with recursively no-parent (-r -np), no containing directory (-nd), skip existing files (-nc)
    try:
        os.system("wget -r -np -nd -nc -P " + nas_folder + " ftp://spm22:newversion@140.105.87.59" + remote_folder)
        return "Files transferred."
    except:
        return "Error in files transfer."

def log_meta(meta_dict):
    """Retrieve some metadata for the logbook
    
    Args:
            meta_dict: dictionary of metadata
    
    Returns: string with selected metadata formatted
    
    """

    meta1 = ""
    meta1 += meta_dict["Date"][9:14]
    meta1 += " "
    if meta_dict["Gap Voltage"].startswith("-"):
        meta1 += "(" + meta_dict["Gap Voltage"].split(" ")[0][0:6] + " " + meta_dict["Gap Voltage"].split(" ")[1] + ", "
    else:
        meta1 += "(" + meta_dict["Gap Voltage"].split(" ")[0][0:5] + " " + meta_dict["Gap Voltage"].split(" ")[1] + ", "
    meta1 += meta_dict["Feedback Set"].split(" ")[0][0:5] + " " + meta_dict["Feedback Set"].split(" ")[1] + ")"
    meta1 += "\n"

    meta2 = " "*4
    meta2 += "(" + meta_dict["X Offset"] + ", " + meta_dict["Y Offset"] + ")"
    meta2 += "\n"

    return meta1, meta2
